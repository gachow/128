
from flask import Flask
from flask import request
from redis import Redis, RedisError
import os
import socket
from flask import Response

app = Flask(__name__)

@app.route("/")
def index():
  return 'No Code'

@app.route('/hello',methods=['GET'])
def hello():
       if request.method== 'GET':
         name = request.args.get('name')
         if not name:
            return "Hello user!"
         else:
            return "Hello " + name + "!" 

@app.route('/check',methods=['GET', 'POST', 'PUT'])
def check():

       if request.method == 'GET':
         content = "This is a GET request"
         resp = Response(content, status=200, mimetype='text/plain')
         return resp
       if request.method == 'POST':
         content = "This is a POST request"
         resp = Response(content, status=200, mimetype='text/plain')
         return resp
       if request.method == 'PUT':
         resp = Response('', status=405, mimetype='text/plain')
         return resp


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
  